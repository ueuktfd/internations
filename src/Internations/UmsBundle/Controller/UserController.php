<?php

namespace Internations\UmsBundle\Controller;

use Internations\UmsBundle\Entity\User;
use Internations\UmsBundle\Form\User\addUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    public function listAction()
    {
        $userRepository = $this->getDoctrine()
            ->getRepository('InternationsUmsBundle:User');
        $users = $userRepository->findAll();
        $newUser = new User();
        $newUserForm = $this->createForm(
            new addUserType(),
            $newUser,
            array('action' => $this->generateUrl('user_add'))
        );

        return $this->render(
            'InternationsUmsBundle:User:list.html.twig',
            array(
                'users' => $users,
                'newUserForm' => $newUserForm->createView()
            )
        );
    }

    public function groupsAction($user_id)
    {
        $userRepository = $this->getDoctrine()
            ->getRepository('InternationsUmsBundle:User');
        $teamRepository = $this->getDoctrine()
            ->getRepository('InternationsUmsBundle:Team');

        $assignedTeamIds = [];

        $user = $userRepository->find($user_id);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $teams = $user->getTeams();
        foreach ($teams as $team) {
            $assignedTeamIds[] = $team->getId();
        }

        $qb = $teamRepository->createQueryBuilder('t');
        if (count($assignedTeamIds) > 0) {
            $qb->where('t.id NOT IN  (' . implode(",", $assignedTeamIds) . ')');
        }

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('user_assign_group', array('user_id' => $user_id)))
            ->add(
                'group',
                'entity',
                array(
                    'class' => 'InternationsUmsBundle:Team',
                    'query_builder' => $qb,
                )
            )

            ->add('save', 'submit', array('label' => 'Assign group'))
            ->getForm();

        return $this->render(
            'InternationsUmsBundle:User:groups.html.twig',
            array(
                'teams' => $teams,
                'user_id'   => $user_id,
                'form' => $form->createView()
            )
        );
    }

    public function addAction(Request $request)
    {
        $newUser = new User();
        $newUserForm = $this->createForm(
            new addUserType(),
            $newUser,
            array('action' => $this->generateUrl('user_add'))
        );
        $newUserForm->handleRequest($request);
        if ($newUserForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newUser);
            $em->flush();
            $session = $request->getSession();
            $session->getFlashBag()->add('notice', 'New user has been added.');

            return $this->redirect($this->generateUrl('user_list'));
        }

        return $this->render(
            'InternationsUmsBundle:User:add.html.twig',
            array(// ...
            )
        );
    }

    public function deleteAction($user_id, Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('InternationsUmsBundle:User');
        $user = $userRepository->find($user_id);
        try {

            if (!$user) {
                throw new \Exception('User not found.');
            }
            $em->remove($user);
            $em->flush();
            $session->getFlashBag()->add('notice', 'User has been deleted');
        } catch (\Exception $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return $this->redirect($this->generateUrl('user_list'));
    }

    public function assignGroupAction($user_id, Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('InternationsUmsBundle:User');
        $teamRepository = $em->getRepository('InternationsUmsBundle:Team');

        try {
            if (empty($request->get("form")['group'])) {
                throw new \Exception('Please select a group.');
            }
            $team_id = (int)$request->get("form")['group'];
            $user = $userRepository->find($user_id);
            $team = $teamRepository->find($team_id);


            if (!$user) {
                throw new \Exception('User not found.');
            }
            if (!$team) {
                throw new \Exception('Group not found.');
            }
            $user->addTeam($team);
            $session->getFlashBag()->add('notice', 'Group has been Added');
            $em->flush();
        } catch (\Exception $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return $this->redirect($this->generateUrl('user_list') . "#additional_info_" . $user_id);
    }

    public function removeGroupAction($user_id, $group_id, Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('InternationsUmsBundle:User');
        $user = $userRepository->find($user_id);
        $teamRepository = $em->getRepository('InternationsUmsBundle:Team');
        $team = $teamRepository->find($group_id);
        try {

            if (!$user) {
                throw new \Exception('User not found.');
            }
            if (!$team) {
                throw new \Exception('Group not found.');
            }
            $user->removeTeam($team);
            $em->flush();
            $session->getFlashBag()->add('notice', 'Group has been removed');
        } catch (\Exception $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('user_list') . "#additional_info_" . $user_id);
    }


}
