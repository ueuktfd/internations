<?php

namespace Internations\UmsBundle\Controller;

use Internations\UmsBundle\Entity\Team;
use Internations\UmsBundle\Form\Team\addTeamType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GroupsController extends Controller
{
    public function listAction()
    {
        $teamRepository = $this->getDoctrine()
            ->getRepository('InternationsUmsBundle:Team');
        $teams = $teamRepository->findAll();

        $newTeam = new Team();
        $newTeamForm = $this->createForm(
            new addTeamType(),
            $newTeam,
            array('action' => $this->generateUrl('groups_add'))
        );

        return $this->render(
            'InternationsUmsBundle:Groups:list.html.twig',
            array(
                'teams' => $teams,
                'newTeamForm' => $newTeamForm->createView()
            )
        );
    }

    public function usersAction($group_id) {
        $userRepository = $this->getDoctrine()
            ->getRepository('InternationsUmsBundle:User');
        $teamRepository = $this->getDoctrine()
            ->getRepository('InternationsUmsBundle:Team');

        $assignedUserIds = [];

        $team = $teamRepository->find($group_id);
        if (!$team) {
            throw new NotFoundHttpException('Group not found');
        }
        $users = $team->getUsers();
        foreach ($users as $user) {
            $assignedUserIds[] = $user->getId();
        }
        $qb = $userRepository->createQueryBuilder('u');
        if (count($assignedUserIds) > 0) {
            $qb->where('u.id NOT IN  (' . implode(",", $assignedUserIds) . ')');
        }

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('groups_assign_user', array('group_id' => $group_id)))
            ->add(
                'user',
                'entity',
                array(
                    'class' => 'InternationsUmsBundle:User',
                    'query_builder' => $qb,
                )
            )

            ->add('save', 'submit', array('label' => 'Add user'))
            ->getForm();

        return $this->render(
            'InternationsUmsBundle:Groups:users.html.twig',
            array(
                'users' => $users,
                'group_id'   => $group_id,
                'form' => $form->createView()
            )
        );
    }

    public function addAction(Request $request)
    {
        $newTeam = new Team();
        $newTeamForm = $this->createForm(
            new addTeamType(),
            $newTeam,
            array('action' => $this->generateUrl('groups_add'))
        );
        $newTeamForm->handleRequest($request);
        if ($newTeamForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newTeam);
            $em->flush();
            $session = $request->getSession();
            $session->getFlashBag()->add('notice', 'New group has been added.');

            return $this->redirect($this->generateUrl('groups_list'));
        }

        return $this->render(
            'InternationsUmsBundle:Groups:add.html.twig',
            array( // ...
            )
        );
    }

    public function deleteAction($group_id, Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $teamRepository = $em->getRepository('InternationsUmsBundle:Team');
        $team = $teamRepository->find($group_id);
        $hash = "";
        try {

            if (!$team) {
                throw new \Exception('Group not exists.');
            }
            if (count($team->getUsers()) > 0) {
                $hash .= "#additional_info_" . $group_id;
                throw new \Exception('Group has an users. You must remove all user from the group first');
            }

            $em->remove($team);
            $em->flush();
            $session->getFlashBag()->add('notice', 'Group has been deleted');
        } catch (\Exception $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('groups_list').$hash);

    }

    public function assignUserAction($group_id, Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('InternationsUmsBundle:User');
        $teamRepository = $em->getRepository('InternationsUmsBundle:Team');

        try {
            if (empty($request->get("form")['user'])) {
                throw new \Exception('Please select an user.');
            }
            $user_id = (int)$request->get("form")['user'];
            $user = $userRepository->find($user_id);
            $team = $teamRepository->find($group_id);


            if (!$user) {
                throw new \Exception('User not found.');
            }
            if (!$team) {
                throw new \Exception('Group not found.');
            }
            $user->addTeam($team);
            $em->flush();
            $session->getFlashBag()->add('notice', 'User has been added');
        } catch (\Exception $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return $this->redirect($this->generateUrl('groups_list') . "#additional_info_" . $group_id);
    }

    public function removeUserAction($user_id, $group_id, Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('InternationsUmsBundle:User');
        $user = $userRepository->find($user_id);
        $teamRepository = $em->getRepository('InternationsUmsBundle:Team');
        $team = $teamRepository->find($group_id);
        try {

            if (!$user) {
                throw new \Exception('User not found.');
            }
            if (!$team) {
                throw new \Exception('Group not found.');
            }
            $user->removeTeam($team);
            $em->flush();
            $session->getFlashBag()->add('notice', 'User has been removed');
        } catch (\Exception $e) {
            $session->getFlashBag()->add('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('groups_list') . "#additional_info_" . $group_id);
    }

}
