Internations test task
======================

This is implementation of technical requirments from internations test task. 
The code uses latest Symfony framework.
For installation please clone or download sources, open application folder and run 
```
	composer update 
```
For checking how it works just start bundled php server with command 

```
php app/console server:run
```
# Simple Domain Model. #
![Domain Model.png](https://bitbucket.org/repo/po7nLL/images/2684823767-Domain%20Model.png)

# Database scheme #
![db model.png](https://bitbucket.org/repo/po7nLL/images/840578740-db%20model.png)

# design of web API. #

## Working with users ##

### Show all users ###
Command: 
```
/user/list
```
Return all users stored in DB.


### Create a new user ###
Command:
```
/user/add/{username}
```
Create a new user with name {username} and store in DB


### Delete user ###
Command:
```
/user/delete/{user_id}
```
Delete user with id {user_id} from DB


### Show groups assigned to user ###
Command:
```
/user/showGroups/{user_id}
```
Display all groups assigned to user with id {user_id}


### Assign group to user  ###
Command:
```
/user/assignGroup/{user_id}/{group_id}
```
Assign group with id {group_id} to user with id {user_id}


### Remove group from assigned groups ###
Command:
```
/user/removeGroup/{user_id}/{group_id}
```
Remove group with id {group_id} from assigned to user with id {user_id}


## Working with groups##

### Show all groups###
Command: 
```
/group/list
```
Return all groups stored in DB.


### Create a new group###
Command:
```
/group/add/{groupname}
```
Create a new group with name {groupname} and store in DB


### Delete group###
Command:
```
/group/delete/{group_id}
```
Delete group with id {group_id} from DB


### Show groups assigned to user ###
Command:
```
/group/showUsers/{group_id}
```
Display all users assigned to group with id {group_id}


### Assign user to group  ###
Command:
```
/group/assignUser/{group_id}/{user_id}
```
Assign user with id {user_id} to group with id {group_id}


### Remove user from assigned users###
Command:
```
/group/removeUser/{group_id}/{user_id}
```
Remove user with id {user_id} from assigned to group with id {group_id}
